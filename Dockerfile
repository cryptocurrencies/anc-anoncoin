# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/Anoncoin/anoncoin.git /opt/anoncoin
RUN cd /opt/anoncoin/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/anoncoin/ && \
    make -j3

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r anoncoin && useradd -r -m -g anoncoin anoncoin
RUN mkdir /data
RUN chown anoncoin:anoncoin /data
COPY --from=build /opt/anoncoin/src/anoncoind /usr/local/bin/anoncoind
COPY --from=build /opt/anoncoin/src/anoncoin-cli /usr/local/bin/anoncoin-cli
USER anoncoin
VOLUME /data
EXPOSE 9377 9376
CMD ["/usr/local/bin/anoncoind", "-datadir=/data", "-conf=/data/anoncoin.conf", "-server", "-txindex", "-printtoconsole"]